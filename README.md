**Projeto de teste automatizado do SuperAPP**

O projeto foi criado utilizando o Appium com CodeceptJS e Cucumber para escrita dos cenários de teste. 

O mapeamentos dos elementos de interface devem seguir o padrão de projeto PageObject.


**Instalação e configuração**

Para executar os testes é necessário instalar os seguintes componentes: 

npm **-->** https://docs.npmjs.com/cli/v6/commands/npm-install

Appium **-->** https://appium.io/docs/en/about-appium/getting-started/?lang=en

Appium-Doctor **-->** https://www.npmjs.com/package/appium-doctor

Android Studio e o SDK Tools **-->** https://developer.android.com/studio

VS Code **-->** https://code.visualstudio.com/download

JAVA **-->** https://www.java.com/pt-BR/download/ie_manual.jsp?locale=pt_BR

Após instalar os componentes será necessário:

Configurar a veriável de ambiente do **JAVA_HOME** (https://www.youtube.com/watch?v=104dNWmM6Rs) e do **ANDROID_HOME** (https://www.youtube.com/watch?v=wnkynX7Yreo)

Criar um device emulado no Android Studio na opção de "Virtual Device Manager" com o nome **Android 11**, com o Android versão 11. 


**Execução**
Caso tenha acabado de clonar o projeto será necessário executar o comando abaixo para baixar as depedências do projeto. 

    npm install

Atualmente temos a possibilidade de executar os testes de forma local com o device emulado no Android Studio porém para isso será necessário iniciar o servidor "Appium", criar a variável de ambiente do **sistema** "NODE_ENV = android_local" e executar o comando no VS Code:

    npm run test

**OBS:** Caso não crie a variável de ambiente os testes serão executados no BrowserStack.

**Observações**

O arquivo *.apk que será testado deve ficar dentro da pasta "apk" no diretório raiz do projeto com o nome "app.apk"

**Extras**

Para facilitar o mapeamento dos elementos do APP. pode-se usar o Appium Inspector. O download do mesmo é necessário que seja feito a parte pois ele foi desacoplado do Appium Desktop. 

Para gerar os Steps automaticamente da Feature  basta executar o comando abaixo: 

    npx codeceptjs gherkin:snippets

Para executar apenas um arquivo *.feature de forma exclusiva basta executar o comando como no exemplo abaixo, onde será executado apenas cenários que estiverem dentro do "login.feature"

    npx codeceptjs run features/login.feature