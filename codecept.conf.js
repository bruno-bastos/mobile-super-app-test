const path1 = require('path');
const absolutePathApp = path1.resolve('./apk/app.apk');
var browserstack = require('browserstack-local');
exports.bs_local = new browserstack.Local();

if(process.env.NODE_ENV === 'android_local') {
  require('dotenv').config({ path: './.env.android_local'})
} else {
  require('dotenv').config({ path: './.env.android'})
}

exports.config = {

  output: './output',
  helpers: {
    Appium: {
      platform: process.env.PLATAFORMA,
      app: process.env.APP === 'local' ? absolutePathApp : process.env.APP,
      'browserstack.local':  process.env.APP !== 'local' ? true : '',
      'browserstack.debug': process.env.APP !== 'local' ? true : '',
      host: process.env.APP !== 'local' ? 'hub-cloud.browserstack.com' : '',
      port: process.env.APP !== 'local' ? 4444 : 4723 ,
      user: process.env.APP !== 'local' ? 'henriqueamaral_c7xPNA' : '' ,
      key: process.env.APP !== 'local' ? 'xoYm7WsnMNYjr7oxzzhf' : '',
      desiredCapabilities: {
        project: 'SuperAPP',
        build : 'Teste automatizado Super APP',
        name : 'Funcional teste SuperAPP',
        appWaitPackage: process.env.PLATAFORMA == 'Android' ? 'br.com.credz.consultaapp' : '',
        appActivity: process.env.PLATAFORMA == 'Android' ?'MainActivity' : '',
        device: process.env.DEVICE,
        platformVersion: process.env.PLATFORM_VERSION,
        noReset: false,
        autoGrantPermissions: true
      }
    }
  },
  include: {
    I: './steps_file.js',
    login_page: './pages/login/login_page.js',
    comprovante_pagamento_page: './pages/comprovante/comprovantes_page.js',
    comprovante_pix_page: './pages/pix/comprovante_pix_page.js',
    util: './pages/util/util.js',
    props: './props'
  },
  mocha: {},
  bootstrap: null,
  timeout: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './features/*.feature',
    steps: [
      './step_definitions/login_steps.js',
      './step_definitions/comprovantes_steps.js', 
      './step_definitions/pix_steps'
    ], 
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    retryFailedStep: {
      enabled: false
    },
    retryTo: {
      enabled: true
    },
    eachElement: {
      enabled: true
    },
    pauseOnFail: {}
  },
  stepTimeout: 0,
  stepTimeoutOverride: [{
      pattern: 'wait.*',
      timeout: 0
    },
    {
      pattern: 'amOnPage',
      timeout: 0
    }
  ],
  name: 'TA_SUPERAPP',
  // tests: './*_test.js',
  puglins: {
    retryFailedStep: {
      enable: false
    },
    screenShotOnFail: {
      enable: true
    }
  }
}