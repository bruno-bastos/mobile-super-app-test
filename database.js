const {Client} = require('pg')
const { client_encoding } = require('pg/lib/defaults')

const client = new Client({
    host: "localhost",
    user: "postgres",
    port: 5432,
    password: "#abc123#",
    database: "teste_automatizado"  
})

module.exports =  {
    executeQuery (query) {
        client.connect()

        client.query(query, (err, res) => {
            try {
                console.log(res.rows);
            } catch (error) {
                console.log(err.message);
            } finally {
                client.end;
            }
        })
    }
}