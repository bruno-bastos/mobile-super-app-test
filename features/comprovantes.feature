#language:pt

Funcionalidade: Comprovante de pagamento
  Eu como usuário, gostaria de acessar o comprovante de pagamento 

   Contexto: Login
     Dado que o usuário já esteja logado no SuperAPP

  Cenario: Deve acessar o comprovante com sucesso
    E clicar no menu serviços
    E clicar no menu comprovantes
    E selecionar o filtro dos ultimos 90 dias
    Quando abrir o comprovante de 'Pagamento de conta' com valor '39,01' e data e hora '05 MAI 14:19'
    Entao deve abrir o comprovante de pagamento com sucesso

  Cenario: Deve validar o comprovante de pagamento dia 05-05-2022 com sucesso
    E acessar o menu comprovantes
    E selecionar o filtro dos ultimos 90 dias
    Quando abrir o comprovante de 'Pagamento de conta' com valor '39,01' e data e hora '05 MAI 14:19'
    Então deve validar os valores
    |var_1                |var_2                                           |
    |Valor total          |39,01                                           |
    |Data do pagamento    |05 MAI 14:19                                    |
    |Custo efetivo mensal |0%                                              |
    |Custo efetivo anual  |0%                                              |
    |Juros parcelamento   |0,00                                            |
    |Taxa de juros mensal |0%                                              |
    |Código da transação  |40be9990-039a-47f8-ba2e-6e0784c2a455            |
    |Emissor              |VIVO SP                                         |
    |Vencimento           |05/05/2022                                      | 
    |Código da fatura     |846000000006390100801005011069010129920077740313|
    |Autenticação         |F1.7A.1E.93.47.AE.C1.81BB.73.72.AC.3F.E9.71.48  |

  Cenario: Deve validar o comprovante de pagamento do dia 03-05-2022 com sucesso
    E acessar o menu comprovantes
    E selecionar o filtro dos ultimos 90 dias
    Quando abrir o comprovante de 'Pagamento de conta' com valor '39,01' e data e hora '03 MAI 16:43'
    Então deve validar os valores
    |var_1                |var_2                                           |
    |Valor total          |39,01                                           |
    |Data do pagamento    |03 MAI 16:43                                    |
    |Custo efetivo mensal |0%                                              |
    |Custo efetivo anual  |0%                                              |
    |Juros parcelamento   |0,00                                            |
    |Taxa de juros mensal |0%                                              |
    |Código da transação  |f4ce9fc0-e2d4-471a-b6ea-250f8971732a            |
    |Emissor              |VIVO SP                                         |
    |Vencimento           |03/05/2022                                      |
    |Código da fatura     |846000000006390100801005011069010129920077740313|
    |Autenticação         |A0.38.94.E9.88.27.8C.E892.85.63.F2.B9.C7.FD.61  |
    |Autorização cartão   |212121                                          |

  Cenario: Deve validar o comprovante de PIX Enviado do dia 04-05-2022 com sucesso
    E acessar o menu comprovantes
    E selecionar o filtro dos ultimos 90 dias
    Quando abrir o comprovante de 'Pix Enviado' com valor '10,00' e data e hora '04 MAI 10:02'
    Então deve validar os valores
    |var_1                |var_2                                           |
    |Valor                |10,00                                           |
    |Data da transferência|04 MAI 10:02:19                                 |
    |Tipo do Pix          |Chave pix                                       |
    |ID Transação         |E121092472022050413023nK2WYoHbJf                |