#language:pt

Funcionalidade: Login 
  Eu como usuário, gostaria de fazer login para acessar o sistema. 

  #Criado o contexto apenas como exemplo de uso para futuras implementações em outros casos de teste. 
  # Contexto: Login
  #   Dado que o usuário já esteja logado no SuperAPP

  Cenario: Deve fazer login com sucesso
    Dado o acesso ao SuperAPP de um usuário que possua cartão
    E informar o CPF '25093413755'
    E informar a senha 'Credz@1'
    Quando clicar em logar 
    Entao deve acessar o superAPP com sucesso

  Cenario:Deve acusar CPF inválido
    Dado o acesso ao SuperAPP de um usuário que possua cartão
    E informar o CPF '16028466777'
    Entao deve apresentar a mensagem 'O CPF informado é inválido'

  Cenario:Deve acusar senha inválida
    Dado o acesso ao SuperAPP de um usuário que possua cartão
    E informar o CPF '25093413755'
    E informar a senha 'Credz@123'
    Quando clicar em logar 
    Entao deve apresentar a mensagem 'Senha informada incorreta.'

  Esquema do Cenário:Deve acusar erro de login e não deve fazer login
    Dado o acesso ao SuperAPP de um usuário que possua cartão
    E informar o CPF '<CPF>'
    Entao deve apresentar a mensagem '<mensagem>'
    Exemplos:
      |CPF        |mensagem                               |
      |16028466777|O CPF informado é inválido             |
      |88837825005|Você ainda não possui um cartão Credz. |