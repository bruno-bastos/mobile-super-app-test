#language:pt

Funcionalidade: pix 
  Eu como usuário, gostaria de visualizar o comprovante de envio pix.

  Contexto: pix
    Dado que o usuário já esteja logado no SuperAPP

  Cenario: Validar a exibição do comprovante PIX
    Quando clico no ícone conta
    E clico no pix enviado
    Então deverá exibir o comprovante do pix enviado


  Cenario: Validar a tela de comprovante 
    Quando acessar o menu comprovantes
    Então deve validar os valores
    |var_1                 |var_2                                          |
    |Data do pagamento     |14/03/2022 09:02:08                            |
    |Tipo de transferência |Pix via Chave Pix                              |
    |Nome                  |Allan                                          |
    |CPF                   |***.845.892.**                                 |
    |Nome                  |Cleide Teste                                   |
    |CPF                   |***.183.270.**                                 |
    |Banco                 |CREDZ ADMINISTRADORA DE CARTOE                 |
    |Chave Pix             |39618327060                                    |
    |ID Transação          |E121092472022031412014MWjRazRiXI               |
