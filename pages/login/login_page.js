const { I } = inject();
const utils  = require("../util/utils");

module.exports = {

  fields:{
    iptCpf: '//android.view.ViewGroup[3]/android.widget.EditText',
    iptSenha: '//android.view.ViewGroup[2]/android.widget.EditText'
  },

  labels:{
    txtCpfInvalido: '//android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]',
    txtHabilitacaoSeguranca:'//android.view.ViewGroup[1]/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView',
    txtPropagandaSeguro: '//android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.widget.TextView'
  },

  buttons:{
    btnPossuoCartao: '//android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.TextView',	
    btnContinuarCpf: '//android.view.ViewGroup[4]/android.widget.TextView',
    btnContinuarSenha: '//android.view.ViewGroup[5]/android.widget.TextView',
    btnDeixarParaDepoisHabilitacaoSeguranca: '//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView',
    btnPularPropagandaSeguro: '//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView'
  },

    async logar(cpf, senha) {
      I.waitForElement(this.buttons.btnPossuoCartao, 50)
      I.tap(this.buttons.btnPossuoCartao)
      I.waitForElement(this.fields.iptCpf, 50)
      I.fillField(this.fields.iptCpf, cpf)
      I.tap(this.buttons.btnContinuarCpf)
      I.waitForElement(this.fields.iptSenha, 50)
      I.fillField(this.fields.iptSenha, senha)
      I.tap(this.buttons.btnContinuarSenha)
      I.waitForElement(this.labels.txtHabilitacaoSeguranca, 50)
      if (await utils.verificarSeOElementoExiste(this.labels.txtHabilitacaoSeguranca)) {
        I.tap(this.buttons.btnDeixarParaDepoisHabilitacaoSeguranca)
      } 
      I.waitForElement(this.labels.txtPropagandaSeguro, 50)
      if (await utils.verificarSeOElementoExiste(this.labels.txtPropagandaSeguro)) {
        I.tap(this.buttons.btnPularPropagandaSeguro)
      }
  }
}
