const { I } = inject();

module.exports = {
    realizarScroll (yInicial, yFinal) {

        if (yInicial === undefined ) { yInicial = 1500 }
        if (yFinal === undefined ) { yFinal = 0 }
        
        I.touchPerform([
            {
                action: 'longPress',
                options: { x: 400, y: yInicial}
            },
            {
                action: 'moveTo',
                options: { x: 400, y: yFinal}
            },
            {action: 'release'}
          ])
    },

    async verificarSeOElementoExiste (element) {
        let numOfElements  = await I.grabNumberOfVisibleElements(element)
        if (numOfElements > 0) {
            return true
        } else {
            return false
        }
    },

    async verificarSeOElementoExisteRealizandoScroll(element) {
        for (var i = 0; i < 5; i++) {
            if (await this.verificarSeOElementoExiste(element)) {
                return true;
            } else {
                this.realizarScroll(1500, 500)
            }    
        }
        return false;
    }
}