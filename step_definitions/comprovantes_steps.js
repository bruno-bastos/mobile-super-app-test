const comprovante_pagamento_page = require("../pages/comprovante/comprovantes_page");
const login_page = require("../pages/login/login_page");
const util = require("../pages/util/utils");
const { I } = inject();

Given('clicar no menu serviços', () => {
    I.waitForElement(comprovante_pagamento_page.buttons.btnservicos,20)
    I.tap(comprovante_pagamento_page.buttons.btnservicos)
  });
  
Given('clicar no menu comprovantes', async () => {
    I.wait(3)
    await util.verificarSeOElementoExisteRealizandoScroll(comprovante_pagamento_page.buttons.btncomprovantes)
    I.waitForElement(comprovante_pagamento_page.buttons.btncomprovantes,10)
    I.tap(comprovante_pagamento_page.buttons.btncomprovantes)
  }); 

  Given('acessar o menu comprovantes', () => {
    comprovante_pagamento_page.acessarTelaComprovantesPagamentos()
  });

  Given('selecionar o filtro dos ultimos 90 dias', () => {
    I.wait(5)
    I.tap(comprovante_pagamento_page.buttons.btnFiltroPeriodo)
    I.waitForElement(comprovante_pagamento_page.buttons.btn90Dias)
    I.tap(comprovante_pagamento_page.buttons.btn90Dias)
  });

When('abrir o comprovante de {string} com valor {string} e data e hora {string}', async (tipo, valor, data) => {
  I.wait(3)
  let element = locate('//android.view.ViewGroup[@content-desc]')
  let numOfElements  = await I.grabNumberOfVisibleElements(element)

  for (var i = 1; i <= numOfElements; i++) {
    let numOfAttributes  = await I.grabNumberOfVisibleElements(locate('//android.view.ViewGroup[@content-desc]').at(i).find('//android.widget.TextView'))

    if(numOfAttributes === 5) {
      tipoComprovante = await I.grabTextFromAll(element.at(i).find('//android.widget.TextView').at(1))
      if(tipoComprovante[0] === tipo) {
        valorComprovante = await I.grabTextFromAll(element.at(i).find('//android.widget.TextView').at(3))
        if(valorComprovante[0] === valor.toString()) {
          dataComprovante =   await I.grabTextFromAll(element.at(i).find('//android.widget.TextView').at(5))
          if(dataComprovante[0] === data){
            element = element.at(i)
            break;
          }
        }
      }
    }
  }  
  I.tap(element)
});
  
When('clicar em uma transação de pagamento na tela de comprovantes', () => {
    I.waitForElement(comprovante_pagamento_page.buttons.btnpagamentoconta,10)
    I.tap(comprovante_pagamento_page.buttons.btnpagamentoconta)
  });
  
Then('deve abrir o comprovante de pagamento com sucesso', () => {
    I.wait(3)
    I.see('Resumo do pagamento')
  });
  
Then('deve validar os valores', async (table) => {
  I.grab

    I.wait(2)
    for (const id in table.rows) {
      if (id < 1) {
        continue; //pular a primeira linha da tabela
      }
  
      // Passando pelas células das linhas
      const cells = table.rows[id].cells;
  
      // Recebendo os valores
      const var_1 = cells[0].value;
      const var_2 = cells[1].value;
      element = {xpath: `//android.widget.TextView[contains(@text, '${cells[0].value}')]`}
      if (await I.grabNumberOfVisibleElements(element) === 0) {
        await util.verificarSeOElementoExisteRealizandoScroll(element)
      }
      I.see(var_2) 
      I.see(var_1)
    }   
  });
