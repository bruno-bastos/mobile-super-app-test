const utils  = require("../pages/util/utils");
const login_page = require("../pages/login/login_page");
const { I } = inject();
const database = require("../database.js")

Given('que o usuário já esteja logado no SuperAPP', async () => {

  // database.executeQuery('select * from teste_mobile')

  await login_page.logar(process.env.USUARIO, process.env.SENHA)
});

Given('o acesso ao SuperAPP de um usuário que possua cartão', () => {
  I.waitForElement(login_page.buttons.btnPossuoCartao, 10)
  I.tap(login_page.buttons.btnPossuoCartao)
});

Given('informar o CPF {string}', (cpf) => {
  I.waitForElement(login_page.fields.iptCpf, 10)
  I.fillField(login_page.fields.iptCpf, cpf)
  I.tap(login_page.buttons.btnContinuarCpf)
});

Given('informar a senha {string}', (senha) => {
  I.waitForElement(login_page.fields.iptSenha, 10)
  I.fillField(login_page.fields.iptSenha, senha)
});

When('clicar em logar', () => {
  I.tap(login_page.buttons.btnContinuarSenha)
});

Then('deve acessar o superAPP com sucesso', async () => {
  I.waitForElement(login_page.labels.txtHabilitacaoSeguranca, 50)
  if (await utils.verificarSeOElementoExiste(login_page.labels.txtHabilitacaoSeguranca)) {
    I.tap(login_page.buttons.btnDeixarParaDepoisHabilitacaoSeguranca)
  }
  I.waitForElement(login_page.labels.txtPropagandaSeguro, 50)
  if (await utils.verificarSeOElementoExiste(login_page.labels.txtPropagandaSeguro)) {
    I.tap(login_page.buttons.btnPularPropagandaSeguro)
  }
  I.wait(2)
  I.see('Olá, TESTE')
});

Then('deve apresentar a mensagem {string}', (msg) => {
  I.wait(3)
  I.see(msg)
});
