const comprovante_pix_page = require("../pages/pix/comprovante_pix_page");
const { I } = inject();


  When('clico no ícone conta', () => {
      I.waitForElement(comprovante_pix_page.buttons.btnConta,30)
      I.tap(comprovante_pix_page.buttons.btnConta)
      I.wait(5)
  });

  When('clico no pix enviado',() =>{
      I.waitForElement(comprovante_pix_page.buttons.btnListaPix,10)
      I.tap(comprovante_pix_page.buttons.btnListaPix)
 
  });

  Then ('deverá exibir o comprovante do pix enviado', () =>{
      I.wait(2)
      I.see('PIX ENVIADO')
  });    

//   When('acessar o menu comprovantes', () => {
//  comprovante_pix_page.acessarComprovantePix()
 
//    });
    
//  Then('deve validar os valores', (table) => { 
//      I.wait(2) 
//      for (const id in table.rows) { 
//        if (id < 1) { 
//          continue; //pular a primeira linha da tabela 
//        }  
 
//        // Passando pelas células das linhas
 
//        const cells = table.rows[id].cells;
  
 
//        // Recebendo os valores
 
//        const var_1 = cells[0].value; 
//        const var_2 = cells[1].value; 
//        I.see(var_1) 
//        I.see(var_2)
 
//      }  
 
//    });
